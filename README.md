# dj-library-upgrader
A tool to help DJs upgrade their music library with higher quality files from online shops.

## Why?
I don't know about you, but I have a massive library of electronic music from the late 90s and early 2000s. I have plenty of shady vinyl and radio rips, lots of low-bitrate stuff. I would very much like to play my beloved old music on my shiny new machinery and work it into my sets, but, for the most part, it just sounds like utter shite. 

Buy the same tracks off Beatport, I hear you say? Well duh. But have you spent any time on Beatport? Or Juno, or any of the other major online shops? They probably have all the tracks you're looking for, but the search feels like pulling teeth. I can barely muster enough patience to put 3-4 tracks in my basket before I rage quit and have to take a few days to cool off. At this rate, it would take me literal years to upgrade a sizeable part of my library. So why not have the machine do all the nitty gritty, while I focus on the mixing?

## How?
Well it's fairly straightforward, really. Just point a script at your library. It will go through your tracks, pick out shitty quality ones (you can define shitty for yourself, my default will be < 320kbps), then look on Beatport and Juno (maybe other shops later on) and search for better quality versions and add the cheapest to your basket. No automatic purchases, that would just be creepy.
